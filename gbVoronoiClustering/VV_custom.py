import matplotlib.pyplot as plt
import numpy as np
import logging
import os

from pymatgen.analysis.defects.utils import TopographyAnalyzer
from pymatgen.core.structure import Structure
from gbVoronoiClustering.r_a_Mode import r_a_Mode
from gbVoronoiClustering.ClusterSpheres import ClusterSpheres
from gbVoronoiClustering.default_methods import filter_nothing, filter_positive, _get_r_a, cluster_criterion_default, _get_r_a_from_atomic_radii, _get_atomic_radii_calculated


class VV_custom:
    """
    Class to determine the Voronoi Vertices and their radii for the given Structure
    """
    def __init__(self,
                 structure,
                 framework_ions = None,
                 cations = [],
                 tol = 0.0001,
                 max_cell_range = 1,
                 check_volume = True,
                 constrained_c_frac = 0.5,
                 thickness = 0.5,
                 name = None

    ):
        """
        Computes all voronoi vertices and saves them in object.
        For parametrization of parameters look at pymatgen.analysis.defects.utils.TopographyAnalyzer
        """
        if name is None:
            name=structure.formula
        self._create_logger(name)
        self.structure = structure
        self.num_sites = structure.num_sites
        self.get_atomic_radii = None
        self.get_r_a_from_atomic_radii = None
        self.tol = tol

        framework_ions = list(structure.symbol_set) if framework_ions is None else framework_ions
        self.logger.info("Compute Voronoi tessellation with TopographyAnalyzer")
        topAnalyzer = TopographyAnalyzer(structure=structure,
                                         framework_ions=framework_ions,
                                         cations=cations,
                                         tol=tol,
                                         max_cell_range=max_cell_range,
                                         check_volume=check_volume,
                                         constrained_c_frac=constrained_c_frac,
                                         thickness=thickness
                                         )

        self.logger.info("Number of Voronoi Vertices is " + str(len(topAnalyzer.vnodes)))
        # compute fractional coordinates of Voronoi vertices
        self.voronoi_vertices = [v.frac_coords for v in topAnalyzer.vnodes]



    def _get_all_r_a(self, structure, vfcoords, tri_simplices):
        """
        This method provides all atomic radii for atom
        based on selected mode
        @param structure:
        @param vfcoords: voronoi vertices
        @param tri_simplices: list of circumscribing 4 (usually for 3d) or more atoms, also the nearest Delaunay points
        @return: list of atomic radii
        """
        r_a = np.zeros(len(vfcoords))
        for s, i in zip(tri_simplices, range(0, len(vfcoords))):
            if self.mode == r_a_Mode.CUSTOM:
                r_a[i] = self.get_r_a(structure.sites, s)
            else:
                if self.get_atomic_radii is not None and self.get_r_a_from_atomic_radii is not None:
                    r_a[i] = _get_r_a(structure.sites, s, self.mode, self.get_atomic_radii, self.get_r_a_from_atomic_radii)
                else:
                    r_a[i] = self.get_r_a(structure.sites, s, self.mode)
        return r_a

    def get_interstitial_clustering(self, mode=r_a_Mode.DEFAULT,
                                    filter_function=filter_positive,
                                    get_atomic_radii=_get_atomic_radii_calculated,
                                    get_r_a_from_atomic_radii=_get_r_a_from_atomic_radii
                                    ):
        self.mode = mode
        self.get_atomic_radii = get_atomic_radii
        self.get_r_a_from_atomic_radii = get_r_a_from_atomic_radii

        self.logger.info("The mode " + str(mode.value) + " is selected")
        self.radii = self.get_radii_with_dist_matrix(self.structure, self.voronoi_vertices)
        if np.any(self.radii < 0):
            self.logger.warning("Some radii are negative")
        self.voronoi_vertices, self.radii = filter_function(self.voronoi_vertices, self.radii)

    def cluster(self, cluster_criterion=cluster_criterion_default):
        """
        Cluster Voronoi Vertices with corresponding radii.
        Can be called only if Voronoi Vertices and radii are already computed
        :param cluster_criterion: criterion for taking to the cluster,
                                  must take structure, centers, radii and i and j indices of atoms
                                  and return True, if the ith and jth atoms should be taken to clustering,
                                  for instance look at default_methods.cluster_criterion_default
        :return: list of sets of clusters indexed by original index
        """
        clusterSphere = ClusterSpheres(self.structure, self.voronoi_vertices, self.radii, cluster_criterion, self.logger)
        list_of_clusters = clusterSphere.cluster()
        self.list_of_clusters = list_of_clusters
        return list_of_clusters

    def append_VV_to_structure(self, el='H'):
        """
        Append Voronoi Vertices as Elements (H per default) to the structure
        :param el: Append Voronoi Vertice as this Element
        :return:
        """
        for i in range(len(self.voronoi_vertices)):
            self.structure.append(el, self.voronoi_vertices[i])

    def add_VV_property(self, list_of_clusters):
        """
        Add Clustering as a property "cluster_id" to the structure, i.e.
        cluster_id == -1 <==> Atom was in the original structure
        cluster_id == 0  <==> Atom was added as VV, but does not build any cluster
        cluster_id >= 1  <==> Atom was added as VV and is in one cluster
        cluster_id's are the same <==> Atoms are in the same cluster
        :param list_of_clusters:
        :return: resulted structure
        """

        cluster_ids = [-1] * self.num_sites + self._get_cluster_ids(list_of_clusters)
        velocities = [[0.0, 0.0, 0.0]] * len(cluster_ids)

        self.structure.add_site_property('cluster_id', cluster_ids)
        self.structure.add_site_property('velocities', velocities) # to avoid an error
        return self.structure

    def _get_cluster_ids(self, list_of_clusters):
        """
        Enumerates all cluster
        :param list_of_clusters: list of clusters
        :return: cluster ids, points in the same cluster have the same ids
        """
        self.logger.info("Number of atoms in cluster that not equal to 1:")
        numberClusters_str = ""
        els = {}
        cluster_counter = 0
        cluster_ids = [0] * len(self.voronoi_vertices)
        for c in list_of_clusters:
            cluster_counter += 1
            for i in c:
                cluster_ids[i] = cluster_counter
            numberClusters = len(c)
            if numberClusters != 1:
                numberClusters_str += str(numberClusters) + ", "

            if numberClusters in els:
                els[numberClusters] += 1
            else:
                els[numberClusters] = 1

        if numberClusters_str != "":
            self.logger.info(numberClusters_str[:-2])  # deletes ", " at the end

        self.logger.info("Frequency of number of clusters:")
        self.logger.info(els)

        return cluster_ids

    def get_structure_from_VV(self):
        return self.structure

    def get_voronoi_tesselation(self, el='H'):
        return Structure(lattice=self.structure.lattice,
                         species=[el for i in range(len(self.voronoi_vertices))],
                         coords=self.voronoi_vertices,
                         site_properties={'cluster_id': self._get_cluster_ids(self.list_of_clusters)}
                         )

    def get_radii_with_dist_matrix(self, structure, voronoi_vertices):
        """
        This method provides the radii with distance matrix
        based on pymatgen.analysis.defects.utils.TopographyAnalyzer.cluster_node()
        @param structure:
        @param voronoi_vertices:
        @return: r_vv = R_vv - r_a from the paper
        """
        #vfcoords = [v.frac_coords for v in topAnalyzer.vnodes]
        vfcoords = voronoi_vertices
        sfcoords = structure.frac_coords
        dist_matrix = structure.lattice.get_all_distances(vfcoords, sfcoords)

        R_vv = np.min(dist_matrix, axis=1)

        # take N nearest neighbors (usually 4) with the same (tolerated) distances
        tol = 1e-6
        sortedArgs = np.argsort(dist_matrix, axis=1)
        sortedEls = np.sort(dist_matrix, axis=1)
        tri_simplices = []
        for el, arg in zip(sortedEls, sortedArgs):
            cur = [arg[0]]
            for i in range(1, len(el)):
                if el[i] - el[i - 1] < tol:
                    cur.append(arg[i])
                else:
                    break
            tri_simplices.append(cur)
        #tri_simplices = np.array([l[:4] for l in np.argsort(dist_matrix, axis=1)]) # works only for four
        self.tri_simplices = tri_simplices
        r_a = self._get_all_r_a(structure, vfcoords, tri_simplices)

        r_vv = R_vv - r_a

        # print distribution of radii, if necessary for debug
        plot_distr = False
        if plot_distr:
            self.plot_radii_distrib(r_vv, R_vv)

        return r_vv

    def plot_radii_distrib(self, r_vv=None, R_vv=None, r_a=None):
        kwargs = dict(density=False, stacked=False)
        if R_vv is not None:
            plt.hist(R_vv, bins='auto', **kwargs, color='b', label='R_vv')
        if r_a is not None:
            plt.hist(r_a, **kwargs, color='g', label='r_a')
        if r_vv is not None:
            plt.hist(r_vv, bins='auto', **kwargs, color='r', label='r_vv')
        plt.gca().set(title='Distribution of radii', ylabel='Frequency')
        plt.xlim(-3, 3)
        plt.legend()
        plt.show()

    def get_convex_polyhedras(self):
        """
        :return: indices of the sites that circumscribe each cluster in the convex hull
        """
        from functools import reduce
        polyhedras = []
        for cluster in self.list_of_clusters:
            polyhedras.append(reduce(lambda x, y: x.union(y),
                                     list(map(set,
                                              [self.tri_simplices[el] for el in cluster]))))
        return polyhedras

    def _create_logger(self, name):
        self.logger = logging.getLogger(name)
        result_folder = os.path.join(os.getcwd(), "results", name)
        os.makedirs(result_folder, exist_ok=True)
        logging.basicConfig(filename=os.path.join(result_folder, name + '.log'),
                            filemode='w',
                            level=logging.INFO,
                            format='[%(asctime)s] [%(levelname)s] => %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S', )
        self.logger.info('Start logging')

    def get_logger(self):
        return self.logger
