import os


class VTPConverter:
    def __init__(self, points, polygons):
        self.points = points
        self.polygons = polygons

    def createByParts(self, filename, mode, datasetname=None):
        result_folder, datasetname = self._find_dirs(filename, datasetname)

        with open(os.path.join(result_folder, filename + mode.value + ".vtp"), "w") as f:
            self._write_setup2(f, datasetname)
            self._write_points(f, self.points)
            self._write_polygons2(f, self.polygons)

    def createDefault(self, filename, mode, datasetname=None):
        result_folder, datasetname = self._find_dirs(filename, datasetname)

        with open(os.path.join(result_folder, filename + mode.value + ".vtp"), "w") as f:
            self._write_setup(f, datasetname)
            self._write_points(f, self.points)
            self._write_polygons(f, self.polygons)

    def createFull(self, filename, mode, datasetname=None):
        result_folder, datasetname = self._find_dirs(filename, datasetname)

        with open(os.path.join(result_folder, filename + mode.value + ".vtp"), "w") as f:
            self._write_setup2(f, datasetname)
            self._write_points(f, self.points)
            self._write_polygons2(f, self.polygons)

    def _find_dirs(self, filename, datasetname):
        os.makedirs(os.path.join(os.getcwd(), "results", filename), exist_ok=True)
        result_folder = os.path.join(os.getcwd(), "results", filename)
        if datasetname is None:
            datasetname = filename
        return result_folder, datasetname

    def _write_setup(self, f, datasetname):
        print("# vtk DataFile Version 2.0",file=f)
        print(datasetname, file=f)
        print("ASCII", file=f)
        print("DATASET POLYDATA", file=f)

    def _write_points(self, f, points):
        print("POINTS", str(len(points)), "double", file=f)
        for point in points:
            print(str(point[0]), str(point[1]), str(point[2]), file=f)

    def _write_polygons(self, f, polygons):
        size = sum(list(map(len, polygons)))
        print("POLYGONS", str(len(polygons)), size, file=f)
        for polygon in polygons:
            print(len(polygon), end=" ", file=f)
            for p in polygon:
                print(str(p), end=" ", file=f)
            print(file=f)

    def _write_setup2(self, f, datasetname):
        print("# vtk DataFile Version 2.0",file=f)
        print(datasetname, file=f)
        print("ASCII", file=f)
        print("DATASET UNSTRUCTURED_GRID", file=f)

    def _write_polygons2(self, f, polygons):
        size = sum(list(map(len, polygons)))
        print("CELLS", str(len(polygons)), size, file=f)
        for polygon in polygons:
            print(len(polygon), end=" ", file=f)
            for p in polygon:
                print(str(p), end=" ", file=f)
            print(file=f)
        print("CELL_TYPES", str(len(polygons)), file=f)
        for i in range(len(polygons)):
            print(5, file=f)
