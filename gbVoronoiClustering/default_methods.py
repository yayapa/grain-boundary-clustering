import numpy as np
import logging

from gbVoronoiClustering.r_a_Mode import r_a_Mode


def filter_positive(voronoi_vertices, radii):
    logging.warning("Filter negatives out (can be deactivated in compute_radii()")
    radii_pos = []
    voronoi_vertices_pos = []
    pos = []
    for i, r in enumerate(radii):
        if r >= 0:
            pos.append(i)
            radii_pos.append(radii[i])
            voronoi_vertices_pos.append(voronoi_vertices[i])
    logging.info("Number of Voronoi Vertices is " + str(len(voronoi_vertices_pos)))
    return np.array(voronoi_vertices_pos), np.array(radii_pos)


def filter_nothing(voronoi_vertices, radii):
    logging.info("Nothing is filtered")
    return voronoi_vertices, radii


def _get_atomic_radii_calculated(sites, tri_simplex):
    atomic_radii = [sites[i].specie.atomic_radius_calculated for i in tri_simplex]
    return atomic_radii


def _get_r_a_from_atomic_radii(atomic_radii, mode):
    if mode == r_a_Mode.DEFAULT:
        return atomic_radii[0]
    elif mode == r_a_Mode.MEAN:
        return sum(atomic_radii) / len(atomic_radii)
    elif mode == r_a_Mode.MAX:
        return max(atomic_radii)
    elif mode == r_a_Mode.MIN:
        return min(atomic_radii)
    elif mode == r_a_Mode.NULL:
        return 0
    else:
        logging.error("INCORRECT MODE in _get_r_a()")


def _get_r_a(sites,
             tri_simplex,
             mode,
             get_atomic_radii=_get_atomic_radii_calculated,
             get_r_a_from_atomic_radii=_get_r_a_from_atomic_radii):
    """
    This method provides an atomic radius for an atom depending on mode
    :param sites: sites of the structure
    :param tri_simplex: circumscribing 4 (usually for 3d) or more atoms, also the nearest Delaunay points
    :param mode: Mode of computing of radius
    :return: an atomic radius
    """
    atomic_radii = get_atomic_radii(sites, tri_simplex)
    return get_r_a_from_atomic_radii(atomic_radii, mode)


def cluster_criterion_default(structure, centers, radii, i, j):
    take_to_cluster = False
    dist_i_j = structure.lattice.get_all_distances([centers[i]], [centers[j]])[0][0]
    if dist_i_j <= radii[i] + radii[j]:
        take_to_cluster = True
    return take_to_cluster
