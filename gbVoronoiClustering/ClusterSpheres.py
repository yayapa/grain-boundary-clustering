import numpy as np
import logging


class ClusterSpheres:
    """
    Cluster the overlapping spheres
    Based on https://github.com/pratikkulkarni228/overlapping-cluster-removal/blob/master/solution/cluster.py
    """
    def __init__(self, structure, centers, radii, cluster_criterion, logger=None):
        self.structure = structure
        self.centers = centers.tolist() if isinstance(centers, np.ndarray) else centers
        self.radii = radii.tolist() if isinstance(radii, np.ndarray) else radii
        self.cluster_criterion = cluster_criterion
        self.logger = logger if logger is not None else logging.getLogger('root')

    def _findOverlap(self):
        '''
        Finds the overlapping spheres and returns a list of tuples of the overlapped spheres.
        '''
        self.logger.info("Start _findOverlap()")
        cluster_list = []
        for i in range(len(self.centers)):
            for j in range(i+1, len(self.centers)):
                if self.cluster_criterion(self.structure, self.centers, self.radii, i, j):
                    cluster_list.append((i, j))

        return cluster_list

    def findClusters(self, edges, vertices):
        '''
        Finds the disjoint sets which
        contain different, individual clusters if present.
        '''

        def makeSet(x):
            return frozenset([x])

        # Create a meta-set/root array to
        sets = set([makeSet(v) for v in vertices])

        # Find a set with element x in a list of all sets
        def findSet(x):
            for subset in sets:
                if x in subset:
                    return subset

        # Form a combined set containing all elements of both sets.
        def setUnion(set1, set2):
            sets.add(frozenset.union(set1, set2))
            sets.remove(set1)
            sets.remove(set2)

        # Finding the connected components
        self.logger.info("Start findClusters(edges, vertices)")
        self.logger.info("Start finding the connected components")
        for (u, v) in edges:
            set1 = findSet(u)
            set2 = findSet(v)

            if set1 != set2:
                setUnion(set1, set2)

        # Create a list of clusters
        # Example: [{3, 4, 5, 6}, {0, 1, 2}], where the elements inside the set denote input indices
        list_of_clusters = []
        self.logger.info("Start creating a list of clusters")
        for individual_sets in sets:
            list_of_clusters.append(set(individual_sets))
        return list_of_clusters

    def cluster(self):
        """
        Cluster the spheres into disjoint sets
        @return: list of sets (clusters)
        """
        # Call our function on input to find overlaps of the spheres
        clusters = self._findOverlap()
        if len(clusters) == 0:
            self.logger.warning("No overlapping elements have been found. Clustering is not possible")
        #with open("overlappingClustersOnlyRidges.txt", "w") as f1:
        #    print(clusters, file=f1)
        # Pass the recently found list of tuples of overlapping spheres as edge while the number of sphere = number of vertices
        vertices = [x for x in range(len(self.centers))]
        list_of_clusters = self.findClusters(clusters, vertices)

        return list_of_clusters
