from enum import Enum


class r_a_Mode(Enum):
    """
    Modes for the determining of r_a
    DEFAULT: the first atomic radius
    MEAN: the mean of all radii
    MAX: the maximum of all radii
    MIN: the minimum of all radii
    NULL: just zero
    """
    DEFAULT = "Default"
    MEAN = "Mean"
    MAX = "Max"
    MIN = "Min"
    NULL = "Null"
    CUSTOM = "Custom"
