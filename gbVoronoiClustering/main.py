import logging
import os
from gbVoronoiClustering.VV_custom import VV_custom
from gbVoronoiClustering.r_a_Mode import r_a_Mode

from pymatgen.io.vasp.outputs import Poscar
from gbVoronoiClustering.pymatgen.pymatgen.io.xyz import EXYZ  # please use also https://github.com/stichri/pymatgen with --branch extended_exyz
from gbVoronoiClustering.VTPConverter import VTPConverter


def save_structure(structure, file_name, mode):
    folder = get_result_dir(file_name)
    os.makedirs(folder, exist_ok=True)
    exyz = EXYZ(structure)
    file_ext = "_clustered"
    final_name = os.path.join(folder, file_name + mode.value + file_ext)
    exyz.write_file(final_name + ".EXYZ")
    Poscar(structure).write_file(final_name + ".POSCAR")


def get_result_dir(name):
    return os.path.join(os.getcwd(), "results", name)


def main():
    input_folder = "./data/"
    file_name = "be17ti2"
    path = input_folder + file_name + ".POSCAR"
    pscr = Poscar.from_file(path)

    structure = pscr.structure
    vorNN = VV_custom(name=file_name, structure=structure)
    mode = r_a_Mode.MEAN
    vorNN.get_interstitial_clustering(mode)
    list_of_clusters = vorNN.cluster()
    vorNN.append_VV_to_structure()
    vorNN.add_VV_property(list_of_clusters)

    # extract polygons
    polyhedras = vorNN.get_convex_polyhedras()
    points = [structure.sites[i].coords for i in range(structure.num_sites)]
    polyhedras = list(map(list, polyhedras))
    vtp = VTPConverter(points=points, polygons=polyhedras)
    vtp.createDefault(file_name, mode)

    save_structure(structure=vorNN.get_voronoi_tesselation(), file_name=file_name, mode=mode)


if __name__ == '__main__':
    main()






