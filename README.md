# Grain Boundary Clustering
## Requirements
```
numpy==1.19.4
matplotlib==3.3.3
pymatgen==2020.11.11
```
The requirements can be installed using *requirements.txt* or *setup.py*

For saving file in *.EXYZ* extension [the separate version of pymatgen](https://github.com/stichri/pymatgen) is used. Please clone the repository into *./gbVoronoiClustering* package and switch to the *extended_exyz* branch
```commandline
git clone --branch extended_xyz https://github.com/stichri/pymatgen
```