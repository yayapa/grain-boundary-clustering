from setuptools import setup
from os.path import join, dirname
import gbVoronoiClustering

setup(
    name='grain-boundary-clustering',
    version=gbVoronoiClustering.__version__,
    packages=['gbVoronoiClustering'],
    install_requires=[
        'numpy'#'numpy==1.19.4'
        'matplotlib'#'matplotlib==3.3.3'
        'pymatgen'#'pymatgen==2020.11.11'  # please use also https://github.com/stichri/pymatgen with --branch extended_exyz
    ],
    url='https://gitlab.com/yayapa/grain-boundary-clustering',
    license='',
    authors='Christopher Stihl and Dmitrii Seletkov',
    author_email='dimaseletkov@gmail.com',
    description='Grain Boundary Voronoi Clustering',
    long_description=open(join(dirname(__file__), 'README.md')).read(),
)
